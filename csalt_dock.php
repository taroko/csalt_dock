<?php

$field = array();
///@brief for post method and cmd method
if(isset($_POST["cmd"]))
{
        $field = $_POST;
}
elseif($argc > 1)
{
	// php this_file.php cmd=run_srnap or php this_file.php cmd=run_viewer 
        for($i=1;$i<$argc;$i++)
        {
                $tmp_arg = explode("=" , $argv[$i]);
                $field[$tmp_arg[0]] = $tmp_arg[1];
        }
}
//var_dump ($field);
$calt_dock = new CsaltDock ($field);


class CsaltDock
{
	//API related repository ID & token
	var $app_platform; //https://cloud-hoth.illumina.com
	var $access_token; //e.g. 1208af54b42e48e0a9c5320e52d7b24d
	var $app_session_id; //e.g. 4024078
	var $project_id;
	var $sample_id;
	var $appresult_id;
	var $debug_level = 1;

	var $input_session_info_path = "/data/input/AppSession.json";///data/input/AppSession.json
	var $download_source_url = "http://www.jhhlab.tw";
	var $run_by_basespace_;
	var $app_type = array();
	var $app_type_list = array ( 'srnap', 'viewer' );
	var $output_json;

	var $app_status;
	var $base_api_url;// = "https://api.basespace.illumina.com/";
	var $create_app_curl_response_info;

	//function __construct($app_type_in)
	function CsaltDock ($app_type_in)
	{
		$this->init($app_type_in);
		$method = $this->app_type["cmd"];
		if(! method_exists($this, $method))
		{
			echo "fucker 5566\n";
			$this->debug("No CMD method '$method' exist.");
		}
		$this->$method();	//distribute among different branch: run_viewer or run_srnap by the selection of cmd
	}

	private function init ($app_type_in)
	{
		$this->app_type = $app_type_in;
		$this->run_by_basespace_ = file_exists ($this->input_session_info_path);

		if ($this->run_by_basespace_)
		{
			$this->app_session_id = getenv('AppSessionId');//e.g. 4024078
			$this->access_token = getenv('AccessToken');//e.g. 1208af54b42e48e0a9c5320e52d7b24d
		}
		else
		{	//viewer_basespace
			$this->app_session_id = 16395459;//4177332;
			$this->access_token = "9b3ecd11dfc04853907514b4774cf682";//"fb39eb2fd6d44736aa7df43a2405c14b";
			//srnap_basespace
		//	$this->app_session_id = 15709707;
		//	$this->access_token = "357a0a60eea148d6bf22e6404536d2c5";
			//srnap_hoth
		//	$this->app_session_id = 4186203;
		//	$this->access_token = "f85b395da812405fa033f1633775cb7c";
		}
	}

	public function run_viewer ()
	{
		echo "run viewer \n";
		if (!$this->run_by_basespace_)
			$this->input_session_info_path ="/genomes/ViewerAppSession.json";//

		$session_properties;
		$this->LoadInputSessionInfo($session_properties);
		$this->ParseFromSessionInputViewer ($session_properties);

		$post_data = Array (
			"Name"				=>  "csalt_viewer",
			"Description"		=>  "provide an integrated web-based interface for app result presentation",
			"HrefAppSession"	=>  $this->output_json['appsessionuri']//,
			);

		$this->CreateAppResult($post_data);
		$this->GetInputFileViewer();
		$output_str = json_encode ($this->output_json);
		$this->CreateConfigFileViewer ("/genomes/Viewerconfig.json", "w", $output_str);
echo "done run_viewer\n";
var_dump ($output_str);
	}

	public function run_srnap ()
	{
		echo "run srnap \n";
		if (!$this->run_by_basespace_)
			$this->input_session_info_path ="/genomes/AppSession.json";

		$session_properties;
		$this->LoadInputSessionInfo($session_properties);
		$this->ParseFromSessionInputSrnap ($session_properties);
		$this->DownloadDatabaseSrnap();

		$post_data = Array (
			"Name"				=>  "small_rna_pipeline",
			"Description"		=>  "take fastq file as input, and provide aligned bam or bigwig files and annotated statistics in tsv as output",
			"HrefAppSession"	=>  $this->output_json['appsessionuri'],
			"References"		=>  Array( Array("Rel" => "using", "HrefContent" => $this->output_json['sample_href']))
			);

		$this->CreateAppResult($post_data);
		$this->GetInputFileSrnap();
		$output_str = json_encode ($this->output_json);
		$this->CreateConfigFileSrnap ("/genomes/config.json", "w", $output_str);
echo "done run_srnap\n";
var_dump ($output_str);
	}

	private	function LoadInputSessionInfo (&$content)
	{ 
		$input_json = $this->LoadFromFile ($this->input_session_info_path, "r");
		$session_info = json_decode ($input_json, true); //parse input_json into properties
		$this->app_platform = $session_info['OriginatingUri']."/";
		$this->base_api_url = "https://api.".substr ($this->app_platform, 8);//api.cloud-hoth.illumina.com/";
		$this->output_json['base_api_url'] = $this->base_api_url;
		$content = $session_info['Properties']['Items'];  //an array of property contents
		$this->app_status = $session_info['Status'];

		$this->output_json['app_platform'] = $this->app_platform;
		$this->output_json['access_token'] = $this->access_token;
		$this->output_json['appsessionuri'] = "v1pre3/appsessions/".$this->app_session_id."/";
	}

	private function CreateConfigFileSrnap ($file_path, $written_mode, $output_str)
	{
		$this->WriteToFile ($file_path, "w", $output_str);
		$options = Array (
				CURLOPT_URL 		=> $this->base_api_url.$this->output_json['app_results']."files?name=config.json",
				CURLOPT_HTTPHEADER 	=> Array("x-access-token: {$this->access_token}", "Content-Type: application/octet-stream"), 
				CURLOPT_RETURNTRANSFER 	=> true,
				CURLOPT_VERBOSE 	=> true,
				CURLOPT_POST 		=> true,
				CURLOPT_POSTFIELDS 	=> $file_path
				);
		$response = $this->ExecuteCurl($options);  
		echo "send config.json response: \n";
		var_dump ($response);
/*
		$curl_addr = $this->base_api_url.$this->output_json['app_results']."files?name=config.json";
		$curl_cmd = "/usr/bin/curl -v -H \"x-access-token: ".$this->access_token."\" -d @/genomes/config.json -H \"Content-Type: application/octet-stream\" \"".$curl_addr."\"";
		echo "system on cmd ".$curl_cmd."\n";
		$last_line = shell_exec($curl_cmd);
		echo "done cmd\tlast_line".$last_line."\nreturn_var".$return_var."\n";
*/
	}

	private function CreateConfigFileViewer ($file_path, $written_mode, $output_str)
	{
		$this->WriteToFile ($file_path, "w", $output_str);
//		$options = Array (
//				CURLOPT_URL 		=> $this->base_api_url.$this->output_json['app_results']."files?name=Viewerconfig.json",
//				CURLOPT_HTTPHEADER 	=> Array("x-access-token: {$this->access_token}", "Content-Type: application/octet-stream"), 
//				CURLOPT_RETURNTRANSFER 	=> true,
//				CURLOPT_VERBOSE 	=> true,
//				CURLOPT_POST 		=> true,
//				CURLOPT_POSTFIELDS 	=> $file_path
//				);
//		$response = $this->ExecuteCurl($options);  
		echo "send config.json response: \n";
		var_dump ($response);
	}

	private	function ParseFromSessionInputViewer (&$content)
	{
		$appresult_href_array = array();
		foreach ($content as $element)
		{
			switch ($element['Name'])
			{
				case 'Input.app-result-id':
					foreach ($element['Items'] as $entry)
						array_push ($appresult_href_array, $entry['Href']."/");
					$this->output_json['input_appresult_href'] = $appresult_href_array;
					break;
				case 'Input.project-id':
					$this->output_json['project_href'] = $element['Content']['Href']."/";
					$this->project_id = $element['Content']['Id'];
					break;
			}
		}
//		echo "Output Json Str\n\n";
//		var_dump ($this->output_json);
	}

	private	function ParseFromSessionInputSrnap (&$content)
	{
		$barcode_seq_array = array();
		foreach ($content as $element)
		{
			switch ($element['Name'])
			{
				case 'Input.adapter_sequence':
					$this->output_json['adapter_sequence'] = $element['Content'];
					break;
				case 'Input.barcode-type':
					$this->output_json['barcode_type'] = $element['Content'];
					break;
				case 'Input.barcode_sequence_1':
					array_push ($barcode_seq_array, $element['Content']);
					break;
				case 'Input.barcode_sequence_2':
					array_push ($barcode_seq_array, $element['Content']);
					break;
				case 'Input.barcode_sequence_3':
					array_push ($barcode_seq_array, $element['Content']);
					break;
				case 'Input.barcode_sequence_4':
					array_push ($barcode_seq_array, $element['Content']);
					break;
				case 'Input.reference_genome':
					$this->output_json['reference_genome'] = $element['Content'];
					break;
				case 'Input.sample-id':
					$this->output_json['sample_href'] = $element['Content']['Href']."/";
					$this->sample_id = $element['Content']['Id'];
					$this->output_json['input_read_count'] = $element['Content']['NumReadsRaw'];
					$this->output_json['paired_end_input'] = $element['Content']['IsPairedEnd'];
					echo "total input read count: ".$this->output_json['sample_href']."\n";
					break;
				case 'Input.project-id':
					$this->output_json['project_href'] = $element['Content']['Href']."/";
					$this->project_id = $element['Content']['Id'];
					break;
				case 'Input.analysis-prefix-name'://'Input.app-session-name':
					$this->output_json['analysis_prefix_name'] = $element['Content'];
					//$this->output_json['app_session_name'] = strtr ($element['Content'], array('/'=>'_', ' '=>'_'));//$element['Content'];
					break;
			}
		}
		$this->output_json['barcode_sequence_vec'] = $barcode_seq_array;
//		echo "Output Json Str\n\n";
//		var_dump ($this->output_json);
	}

	private function DownloadDatabaseSrnap ()
	{
		$genome = $this->output_json['reference_genome'];
		if($genome == "")
			die("no genome");
		$list = $this->GetDownloadFileList($genome);
		$name_list = array ("ENSEMBL.all.bed", "ENSEMBL.miRNA-3p5p.bed");
		$this->DownloadImpl($this->download_source_url, $genome, $list, $name_list);
	}

	private	function GetDownloadFileList($g)
	{
		return array(
				"$g.chrLen"
				, "$g.chrStart"
				, "$g.NposLen.z"
				, "$g.t_table.bwt"
				, "ENSEMBL.$g.all.bed"
				, "ENSEMBL.$g.miRNA-3p5p.bed"
				, "$g.fa"
				//, "{$g}_random.fa"
			    );
	}

	private	function DownloadImpl($url, $g, $list, $name_list)
	{
		$cmd = "cd /genomes/;";
		echo "$cmd\n";
		shell_exec($cmd);

		foreach($list as $l)
		{
			if(file_exists($l))
				continue;
			$link = "$url/pipeline_index/$g/$l";
			$cmd = "lftp -c \"pget -n 4 $link\";";
			//$cmd = "cd /mnt;sudo lftp -c \"pget -n 4 $link\";";
			echo "$cmd\n";
			if ($this->run_by_basespace_)
				shell_exec($cmd);


			if (strpos ($l, "all.bed"))
			{
				$cmd = "mv $l $name_list[0]";
				echo "$cmd\n";
				shell_exec($cmd);
			}

			else if (strpos ($l, "miRNA-3p5p.bed"))
			{
				$cmd = "mv $l $name_list[1]";
				echo "$cmd\n";
				shell_exec($cmd);
			}

		}

		$download_file_list = array(
			"genome_path"		=>	"/genomes/$g.fa", 
			"database_path1"	=>	"/genomes/ENSEMBL.all.bed",
			"database_path2"	=>	"/genomes/ENSEMBL.miRNA-3p5p.bed",
			"index_prefix"		=>	"/genomes/$g"
			);
		//$cmd = "chmod 777 -R /mnt";
		//shell_exec($cmd);
		//$cmd = "ln -s /mnt/* .";
		//shell_exec($cmd);
		$this->output_json['download_file'] = $download_file_list;
echo "Download file: \n";
var_dump ($this->output_json['download_file']);
	}

 	private function CreateAppResult ($post_data)
	{
		$options = Array (
				CURLOPT_URL				=> $this->base_api_url.$this->output_json['project_href']."/appresults",
				CURLOPT_HTTPHEADER		=> Array("x-access-token: {$this->access_token}", "Content-Type: application/json"),
				CURLOPT_RETURNTRANSFER 	=> true,
				CURLOPT_VERBOSE			=> true,
				CURLOPT_POST			=> true,
				CURLOPT_POSTFIELDS		=> json_encode($post_data)
				);
		$create_app_curl_response = $this->ExecuteCurl($options);  
		$this->create_app_curl_response_info = json_decode ($create_app_curl_response);
		$this->output_json['app_results'] = $this->create_app_curl_response_info->Response->Href."/";
		$this->appresult_id = $this->create_app_curl_response_info->Response->Id;
var_dump ($this->output_json);
	}
	
	private function GetInputFileViewer ()
	{
		$file_list_tmp = array();
		$file_type_list = array (".des3", ".bam", ".bwg", ".bai");

		foreach ($this->output_json['input_appresult_href'] as $element1)
		{
			$file_list=array ();
			$index=0;
			foreach ($file_type_list as $type)
			{
				$results = $this->GetSimpleUrl ("{$this->base_api_url}/{$element1}/files?SortBy=Path&SortDir=Asc&Limit=1000&Extensions=".$type);
				foreach ($results["Response"]["Items"] as $element2)
				{
					$file_list[$index] = array (
							"Name"		=> $element2["Name"],
							"Dir"		=> strtr($element2["Path"], array ($element2["Name"] => "")),
							"Content"	=> $element2["Href"]."/",
							"Size"		=> $element2["Size"]
							);
					++$index;
				}
			}
//echo "current appresult".$element1."\n";
//var_dump ($file_list);
			array_push ($file_list_tmp, $file_list);
		}
		$this->output_json["file_list"] = $file_list_tmp;
//		echo "obtained output_json \n";
//		var_dump ($this->output_json);
	}

	private function GetInputFileSrnap ()
	{
		$results;
		$results = $this->GetSimpleUrl("{$this->base_api_url}/{$this->output_json['sample_href']}/files?Limit=1000");

		$file_list=array ();
		$index=0;
		foreach ($results["Response"]["Items"] as $element)
		{
			$file_list[$index] = array (
				"Name"		=> $element["Name"],
				"Content"	=> $element["Href"]."/",
				"Size"		=> $element["Size"]
				);
			++$index;
		}
		$this->output_json["file_list"] = $file_list;
		var_dump ($this->output_json);
	}

	private function GetSimpleUrl($url)
	{
//echo "\ncurl simple url:\t".$url."\n";
		$options = Array(
				CURLOPT_URL => $url,
				CURLOPT_HTTPHEADER => Array("x-access-token: {$this->access_token}"),
				CURLOPT_VERBOSE => true,
				CURLOPT_RETURNTRANSFER => true
				);
		$response = $this->ExecuteCurl($options);
		$results = json_decode($response, true);
//echo "\ncurl simple url response\n ";
//var_dump ($results);
//echo "\n========================\n ";
		return $results;
	}

	private function LoadFromFile ($load_file_path, $mode)
	{
		$load_content = "";
		$file = fopen($load_file_path, $mode);
		while (!feof($file))
		{
			$value = fgets($file);
			$load_content = $load_content.$value;
		}
		fclose($file);
		return $load_content;
	}

	private function ExecuteCurl($opts)
	{  
		$ch = curl_init();
		curl_setopt_array($ch, $opts);
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}

	private function WriteToFile ($write_file_path, $mode, $output_str)
	{
		$file = fopen($write_file_path, $mode);
		fwrite ($file, $output_str);
		fclose($file);
	}

	private function check_field()
	{
		if(count($this->app_type) == 0)
		{
			$this->debug("No cmds.");
		}
		if(! isset($this->app_type["cmd"]))
		{
			$this->debug("No CMD control.");
		}
	}

	private function debug($msg, $title="debug", $level=1)
	{
		if($level > $this->debug_level)
			return;
		echo "[CsaltVisualizer][$title][$level] - $msg\n";
		if($level == 1)
			die();
	}
};

?>
